# Google Drive

Vendor: Google
Homepage: https://www.google.com/

Product: Drive
Product Page: https://www.google.com/drive/

## Introduction
We classify Google Drive into the Data Storage domain as the Google Drive adapter allows access to resources from Google Drive, a cloud and file sharing platform. 

## Why Integrate
The Google Drive adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Google Drive. With this adapter you have the ability to perform operations on items such as:

- Files
- Drives
- Changes
- Channels
- Apps

## Additional Product Documentation
The [API documents for Google Drive](https://developers.google.com/drive/api/reference/rest/v3)