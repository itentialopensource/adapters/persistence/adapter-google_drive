# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Google_drive System. The API that was used to build the adapter for Google_drive is usually available in the report directory of this adapter. The adapter utilizes the Google_drive API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Google Drive adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Google Drive. With this adapter you have the ability to perform operations on items such as:

- Files
- Drives
- Changes
- Channels
- Apps

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
