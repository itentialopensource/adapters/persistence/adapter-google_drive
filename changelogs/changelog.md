
## 0.3.0 [03-20-2023]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-google_drive!1

---

## 0.2.0 [06-01-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-google_drive!1

---

## 0.1.1 [03-17-2022]

- Initial Commit

See commit 58e3fbb

---
