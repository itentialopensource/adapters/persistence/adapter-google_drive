/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-google_drive',
      type: 'GoogleDrive',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const GoogleDrive = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Google_drive Adapter Test', () => {
  describe('GoogleDrive Class Tests', () => {
    const a = new GoogleDrive(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    describe('#driveAboutGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveAboutGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('About', 'driveAboutGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changesPageToken = 'fakedata';
    const changesDriveChangesWatchBodyParam = {
      address: 'string',
      expiration: 'string',
      id: 'string',
      kind: 'api#channel',
      params: {},
      payload: true,
      resourceId: 'string',
      resourceUri: 'string',
      token: 'string',
      type: 'string'
    };
    describe('#driveChangesWatch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveChangesWatch(null, null, null, null, null, null, null, changesPageToken, null, null, null, null, null, null, null, null, null, null, null, null, changesDriveChangesWatchBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Changes', 'driveChangesWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveChangesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveChangesList(null, null, null, null, null, null, null, changesPageToken, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Changes', 'driveChangesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveChangesGetStartPageToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveChangesGetStartPageToken(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Changes', 'driveChangesGetStartPageToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const channelsDriveChannelsStopBodyParam = {
      address: 'string',
      expiration: 'string',
      id: 'string',
      kind: 'api#channel',
      params: {},
      payload: true,
      resourceId: 'string',
      resourceUri: 'string',
      token: 'string',
      type: 'string'
    };
    describe('#driveChannelsStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveChannelsStop(null, null, null, null, null, null, null, channelsDriveChannelsStopBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Channels', 'driveChannelsStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const drivesRequestId = 'fakedata';
    const drivesDriveDrivesCreateBodyParam = {
      backgroundImageFile: {
        id: 'string',
        width: 7,
        xCoordinate: 9,
        yCoordinate: 6
      },
      backgroundImageLink: 'string',
      capabilities: {
        canAddChildren: true,
        canChangeCopyRequiresWriterPermissionRestriction: true,
        canChangeDomainUsersOnlyRestriction: true,
        canChangeDriveBackground: true,
        canChangeDriveMembersOnlyRestriction: true,
        canComment: true,
        canCopy: false,
        canDeleteChildren: false,
        canDeleteDrive: false,
        canDownload: true,
        canEdit: true,
        canListChildren: true,
        canManageMembers: true,
        canReadRevisions: false,
        canRename: true,
        canRenameDrive: true,
        canShare: true,
        canTrashChildren: true
      },
      colorRgb: 'string',
      createdTime: 'string',
      hidden: true,
      id: 'string',
      kind: 'drive#drive',
      name: 'string',
      orgUnitId: 'string',
      restrictions: {
        adminManagedRestrictions: true,
        copyRequiresWriterPermission: true,
        domainUsersOnly: true,
        driveMembersOnly: false
      },
      themeId: 'string'
    };
    describe('#driveDrivesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesCreate(null, null, null, null, null, null, null, drivesRequestId, drivesDriveDrivesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const drivesDriveId = 'fakedata';
    describe('#driveDrivesHide - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesHide(null, null, null, null, null, null, null, drivesDriveId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesHide', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveDrivesUnhide - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesUnhide(null, null, null, null, null, null, null, drivesDriveId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesUnhide', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveDrivesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesList(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const drivesDriveDrivesUpdateBodyParam = {
      backgroundImageFile: {
        id: 'string',
        width: 5,
        xCoordinate: 2,
        yCoordinate: 2
      },
      backgroundImageLink: 'string',
      capabilities: {
        canAddChildren: false,
        canChangeCopyRequiresWriterPermissionRestriction: false,
        canChangeDomainUsersOnlyRestriction: false,
        canChangeDriveBackground: false,
        canChangeDriveMembersOnlyRestriction: true,
        canComment: false,
        canCopy: false,
        canDeleteChildren: true,
        canDeleteDrive: false,
        canDownload: false,
        canEdit: true,
        canListChildren: true,
        canManageMembers: false,
        canReadRevisions: true,
        canRename: true,
        canRenameDrive: true,
        canShare: false,
        canTrashChildren: false
      },
      colorRgb: 'string',
      createdTime: 'string',
      hidden: true,
      id: 'string',
      kind: 'drive#drive',
      name: 'string',
      orgUnitId: 'string',
      restrictions: {
        adminManagedRestrictions: false,
        copyRequiresWriterPermission: true,
        domainUsersOnly: true,
        driveMembersOnly: false
      },
      themeId: 'string'
    };
    describe('#driveDrivesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesUpdate(null, null, null, null, null, null, null, drivesDriveId, null, drivesDriveDrivesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveDrivesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesGet(null, null, null, null, null, null, null, drivesDriveId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesDriveFilesCreateBodyParam = {
      appProperties: {},
      capabilities: {
        canAcceptOwnership: false,
        canAddChildren: false,
        canAddFolderFromAnotherDrive: true,
        canAddMyDriveParent: true,
        canChangeCopyRequiresWriterPermission: true,
        canChangeSecurityUpdateEnabled: true,
        canChangeViewersCanCopyContent: false,
        canComment: false,
        canCopy: true,
        canDelete: true,
        canDeleteChildren: false,
        canDownload: false,
        canEdit: false,
        canListChildren: false,
        canModifyContent: false,
        canModifyContentRestriction: false,
        canMoveChildrenOutOfDrive: true,
        canMoveChildrenOutOfTeamDrive: false,
        canMoveChildrenWithinDrive: true,
        canMoveChildrenWithinTeamDrive: true,
        canMoveItemIntoTeamDrive: true,
        canMoveItemOutOfDrive: true,
        canMoveItemOutOfTeamDrive: false,
        canMoveItemWithinDrive: true,
        canMoveItemWithinTeamDrive: true,
        canMoveTeamDriveItem: true,
        canReadDrive: false,
        canReadRevisions: false,
        canReadTeamDrive: true,
        canRemoveChildren: false,
        canRemoveMyDriveParent: false,
        canRename: true,
        canShare: true,
        canTrash: false,
        canTrashChildren: true,
        canUntrash: false
      },
      contentHints: {
        indexableText: 'string',
        thumbnail: {
          image: 'string',
          mimeType: 'string'
        }
      },
      contentRestrictions: [
        {
          readOnly: false,
          reason: 'string',
          restrictingUser: {
            displayName: 'string',
            emailAddress: 'string',
            kind: 'drive#user',
            me: false,
            permissionId: 'string',
            photoLink: 'string'
          },
          restrictionTime: 'string',
          type: 'string'
        }
      ],
      copyRequiresWriterPermission: false,
      createdTime: 'string',
      description: 'string',
      driveId: 'string',
      explicitlyTrashed: false,
      exportLinks: {},
      fileExtension: 'string',
      folderColorRgb: 'string',
      fullFileExtension: 'string',
      hasAugmentedPermissions: false,
      hasThumbnail: false,
      headRevisionId: 'string',
      iconLink: 'string',
      id: 'string',
      imageMediaMetadata: {
        aperture: 1,
        cameraMake: 'string',
        cameraModel: 'string',
        colorSpace: 'string',
        exposureBias: 2,
        exposureMode: 'string',
        exposureTime: 1,
        flashUsed: true,
        focalLength: 6,
        height: 2,
        isoSpeed: 2,
        lens: 'string',
        location: {
          altitude: 1,
          latitude: 10,
          longitude: 3
        },
        maxApertureValue: 10,
        meteringMode: 'string',
        rotation: 3,
        sensor: 'string',
        subjectDistance: 5,
        time: 'string',
        whiteBalance: 'string',
        width: 10
      },
      isAppAuthorized: false,
      kind: 'drive#file',
      lastModifyingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      linkShareMetadata: {
        securityUpdateEligible: false,
        securityUpdateEnabled: true
      },
      md5Checksum: 'string',
      mimeType: 'string',
      modifiedByMe: false,
      modifiedByMeTime: 'string',
      modifiedTime: 'string',
      name: 'string',
      originalFilename: 'string',
      ownedByMe: false,
      owners: [
        {
          displayName: 'string',
          emailAddress: 'string',
          kind: 'drive#user',
          me: false,
          permissionId: 'string',
          photoLink: 'string'
        }
      ],
      parents: [
        'string'
      ],
      permissionIds: [
        'string'
      ],
      permissions: [
        {
          allowFileDiscovery: true,
          deleted: false,
          displayName: 'string',
          domain: 'string',
          emailAddress: 'string',
          expirationTime: 'string',
          id: 'string',
          kind: 'drive#permission',
          pendingOwner: true,
          permissionDetails: [
            {
              inherited: true,
              inheritedFrom: 'string',
              permissionType: 'string',
              role: 'string'
            }
          ],
          photoLink: 'string',
          role: 'string',
          teamDrivePermissionDetails: [
            {
              inherited: true,
              inheritedFrom: 'string',
              role: 'string',
              teamDrivePermissionType: 'string'
            }
          ],
          type: 'string',
          view: 'string'
        }
      ],
      properties: {},
      quotaBytesUsed: 'string',
      resourceKey: 'string',
      shared: true,
      sharedWithMeTime: 'string',
      sharingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      shortcutDetails: {
        targetId: 'string',
        targetMimeType: 'string',
        targetResourceKey: 'string'
      },
      size: 'string',
      spaces: [
        'string'
      ],
      starred: true,
      teamDriveId: 'string',
      thumbnailLink: 'string',
      thumbnailVersion: 'string',
      trashed: false,
      trashedTime: 'string',
      trashingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: true,
        permissionId: 'string',
        photoLink: 'string'
      },
      version: 'string',
      videoMediaMetadata: {
        durationMillis: 'string',
        height: 8,
        width: 8
      },
      viewedByMe: false,
      viewedByMeTime: 'string',
      viewersCanCopyContent: true,
      webContentLink: 'string',
      webViewLink: 'string',
      writersCanShare: false
    };
    describe('#driveFilesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesCreate(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filesDriveFilesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesFileId = 'fakedata';
    const filesDriveFilesCopyBodyParam = {
      appProperties: {},
      capabilities: {
        canAcceptOwnership: false,
        canAddChildren: true,
        canAddFolderFromAnotherDrive: false,
        canAddMyDriveParent: false,
        canChangeCopyRequiresWriterPermission: false,
        canChangeSecurityUpdateEnabled: false,
        canChangeViewersCanCopyContent: false,
        canComment: true,
        canCopy: true,
        canDelete: false,
        canDeleteChildren: false,
        canDownload: false,
        canEdit: true,
        canListChildren: false,
        canModifyContent: false,
        canModifyContentRestriction: true,
        canMoveChildrenOutOfDrive: true,
        canMoveChildrenOutOfTeamDrive: true,
        canMoveChildrenWithinDrive: false,
        canMoveChildrenWithinTeamDrive: false,
        canMoveItemIntoTeamDrive: false,
        canMoveItemOutOfDrive: false,
        canMoveItemOutOfTeamDrive: true,
        canMoveItemWithinDrive: true,
        canMoveItemWithinTeamDrive: true,
        canMoveTeamDriveItem: false,
        canReadDrive: false,
        canReadRevisions: true,
        canReadTeamDrive: true,
        canRemoveChildren: false,
        canRemoveMyDriveParent: false,
        canRename: false,
        canShare: false,
        canTrash: false,
        canTrashChildren: false,
        canUntrash: true
      },
      contentHints: {
        indexableText: 'string',
        thumbnail: {
          image: 'string',
          mimeType: 'string'
        }
      },
      contentRestrictions: [
        {
          readOnly: true,
          reason: 'string',
          restrictingUser: {
            displayName: 'string',
            emailAddress: 'string',
            kind: 'drive#user',
            me: false,
            permissionId: 'string',
            photoLink: 'string'
          },
          restrictionTime: 'string',
          type: 'string'
        }
      ],
      copyRequiresWriterPermission: true,
      createdTime: 'string',
      description: 'string',
      driveId: 'string',
      explicitlyTrashed: true,
      exportLinks: {},
      fileExtension: 'string',
      folderColorRgb: 'string',
      fullFileExtension: 'string',
      hasAugmentedPermissions: true,
      hasThumbnail: false,
      headRevisionId: 'string',
      iconLink: 'string',
      id: 'string',
      imageMediaMetadata: {
        aperture: 2,
        cameraMake: 'string',
        cameraModel: 'string',
        colorSpace: 'string',
        exposureBias: 7,
        exposureMode: 'string',
        exposureTime: 3,
        flashUsed: false,
        focalLength: 7,
        height: 3,
        isoSpeed: 1,
        lens: 'string',
        location: {
          altitude: 3,
          latitude: 6,
          longitude: 8
        },
        maxApertureValue: 6,
        meteringMode: 'string',
        rotation: 4,
        sensor: 'string',
        subjectDistance: 3,
        time: 'string',
        whiteBalance: 'string',
        width: 7
      },
      isAppAuthorized: true,
      kind: 'drive#file',
      lastModifyingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      linkShareMetadata: {
        securityUpdateEligible: true,
        securityUpdateEnabled: true
      },
      md5Checksum: 'string',
      mimeType: 'string',
      modifiedByMe: false,
      modifiedByMeTime: 'string',
      modifiedTime: 'string',
      name: 'string',
      originalFilename: 'string',
      ownedByMe: false,
      owners: [
        {
          displayName: 'string',
          emailAddress: 'string',
          kind: 'drive#user',
          me: true,
          permissionId: 'string',
          photoLink: 'string'
        }
      ],
      parents: [
        'string'
      ],
      permissionIds: [
        'string'
      ],
      permissions: [
        {
          allowFileDiscovery: false,
          deleted: true,
          displayName: 'string',
          domain: 'string',
          emailAddress: 'string',
          expirationTime: 'string',
          id: 'string',
          kind: 'drive#permission',
          pendingOwner: true,
          permissionDetails: [
            {
              inherited: false,
              inheritedFrom: 'string',
              permissionType: 'string',
              role: 'string'
            }
          ],
          photoLink: 'string',
          role: 'string',
          teamDrivePermissionDetails: [
            {
              inherited: true,
              inheritedFrom: 'string',
              role: 'string',
              teamDrivePermissionType: 'string'
            }
          ],
          type: 'string',
          view: 'string'
        }
      ],
      properties: {},
      quotaBytesUsed: 'string',
      resourceKey: 'string',
      shared: false,
      sharedWithMeTime: 'string',
      sharingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: true,
        permissionId: 'string',
        photoLink: 'string'
      },
      shortcutDetails: {
        targetId: 'string',
        targetMimeType: 'string',
        targetResourceKey: 'string'
      },
      size: 'string',
      spaces: [
        'string'
      ],
      starred: false,
      teamDriveId: 'string',
      thumbnailLink: 'string',
      thumbnailVersion: 'string',
      trashed: false,
      trashedTime: 'string',
      trashingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      version: 'string',
      videoMediaMetadata: {
        durationMillis: 'string',
        height: 5,
        width: 8
      },
      viewedByMe: true,
      viewedByMeTime: 'string',
      viewersCanCopyContent: false,
      webContentLink: 'string',
      webViewLink: 'string',
      writersCanShare: true
    };
    describe('#driveFilesCopy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesCopy(null, null, null, null, null, null, null, filesFileId, null, null, null, null, null, null, null, filesDriveFilesCopyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesCopy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesDriveFilesWatchBodyParam = {
      address: 'string',
      expiration: 'string',
      id: 'string',
      kind: 'api#channel',
      params: {},
      payload: false,
      resourceId: 'string',
      resourceUri: 'string',
      token: 'string',
      type: 'string'
    };
    describe('#driveFilesWatch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesWatch(null, null, null, null, null, null, null, filesFileId, null, null, null, null, filesDriveFilesWatchBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveFilesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveFilesGenerateIds - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesGenerateIds(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesGenerateIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesDriveFilesUpdateBodyParam = {
      appProperties: {},
      capabilities: {
        canAcceptOwnership: true,
        canAddChildren: false,
        canAddFolderFromAnotherDrive: false,
        canAddMyDriveParent: true,
        canChangeCopyRequiresWriterPermission: true,
        canChangeSecurityUpdateEnabled: true,
        canChangeViewersCanCopyContent: false,
        canComment: false,
        canCopy: true,
        canDelete: false,
        canDeleteChildren: false,
        canDownload: false,
        canEdit: true,
        canListChildren: true,
        canModifyContent: false,
        canModifyContentRestriction: true,
        canMoveChildrenOutOfDrive: true,
        canMoveChildrenOutOfTeamDrive: false,
        canMoveChildrenWithinDrive: true,
        canMoveChildrenWithinTeamDrive: false,
        canMoveItemIntoTeamDrive: true,
        canMoveItemOutOfDrive: false,
        canMoveItemOutOfTeamDrive: false,
        canMoveItemWithinDrive: true,
        canMoveItemWithinTeamDrive: false,
        canMoveTeamDriveItem: false,
        canReadDrive: false,
        canReadRevisions: true,
        canReadTeamDrive: true,
        canRemoveChildren: true,
        canRemoveMyDriveParent: true,
        canRename: false,
        canShare: true,
        canTrash: false,
        canTrashChildren: false,
        canUntrash: false
      },
      contentHints: {
        indexableText: 'string',
        thumbnail: {
          image: 'string',
          mimeType: 'string'
        }
      },
      contentRestrictions: [
        {
          readOnly: true,
          reason: 'string',
          restrictingUser: {
            displayName: 'string',
            emailAddress: 'string',
            kind: 'drive#user',
            me: false,
            permissionId: 'string',
            photoLink: 'string'
          },
          restrictionTime: 'string',
          type: 'string'
        }
      ],
      copyRequiresWriterPermission: false,
      createdTime: 'string',
      description: 'string',
      driveId: 'string',
      explicitlyTrashed: false,
      exportLinks: {},
      fileExtension: 'string',
      folderColorRgb: 'string',
      fullFileExtension: 'string',
      hasAugmentedPermissions: true,
      hasThumbnail: false,
      headRevisionId: 'string',
      iconLink: 'string',
      id: 'string',
      imageMediaMetadata: {
        aperture: 3,
        cameraMake: 'string',
        cameraModel: 'string',
        colorSpace: 'string',
        exposureBias: 10,
        exposureMode: 'string',
        exposureTime: 2,
        flashUsed: true,
        focalLength: 6,
        height: 1,
        isoSpeed: 1,
        lens: 'string',
        location: {
          altitude: 2,
          latitude: 5,
          longitude: 9
        },
        maxApertureValue: 8,
        meteringMode: 'string',
        rotation: 4,
        sensor: 'string',
        subjectDistance: 4,
        time: 'string',
        whiteBalance: 'string',
        width: 1
      },
      isAppAuthorized: true,
      kind: 'drive#file',
      lastModifyingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: true,
        permissionId: 'string',
        photoLink: 'string'
      },
      linkShareMetadata: {
        securityUpdateEligible: false,
        securityUpdateEnabled: true
      },
      md5Checksum: 'string',
      mimeType: 'string',
      modifiedByMe: false,
      modifiedByMeTime: 'string',
      modifiedTime: 'string',
      name: 'string',
      originalFilename: 'string',
      ownedByMe: false,
      owners: [
        {
          displayName: 'string',
          emailAddress: 'string',
          kind: 'drive#user',
          me: true,
          permissionId: 'string',
          photoLink: 'string'
        }
      ],
      parents: [
        'string'
      ],
      permissionIds: [
        'string'
      ],
      permissions: [
        {
          allowFileDiscovery: true,
          deleted: false,
          displayName: 'string',
          domain: 'string',
          emailAddress: 'string',
          expirationTime: 'string',
          id: 'string',
          kind: 'drive#permission',
          pendingOwner: true,
          permissionDetails: [
            {
              inherited: true,
              inheritedFrom: 'string',
              permissionType: 'string',
              role: 'string'
            }
          ],
          photoLink: 'string',
          role: 'string',
          teamDrivePermissionDetails: [
            {
              inherited: true,
              inheritedFrom: 'string',
              role: 'string',
              teamDrivePermissionType: 'string'
            }
          ],
          type: 'string',
          view: 'string'
        }
      ],
      properties: {},
      quotaBytesUsed: 'string',
      resourceKey: 'string',
      shared: false,
      sharedWithMeTime: 'string',
      sharingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      shortcutDetails: {
        targetId: 'string',
        targetMimeType: 'string',
        targetResourceKey: 'string'
      },
      size: 'string',
      spaces: [
        'string'
      ],
      starred: false,
      teamDriveId: 'string',
      thumbnailLink: 'string',
      thumbnailVersion: 'string',
      trashed: false,
      trashedTime: 'string',
      trashingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      version: 'string',
      videoMediaMetadata: {
        durationMillis: 'string',
        height: 4,
        width: 7
      },
      viewedByMe: true,
      viewedByMeTime: 'string',
      viewersCanCopyContent: false,
      webContentLink: 'string',
      webViewLink: 'string',
      writersCanShare: false
    };
    describe('#driveFilesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesUpdate(null, null, null, null, null, null, null, filesFileId, null, null, null, null, null, null, null, null, null, filesDriveFilesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveFilesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesGet(null, null, null, null, null, null, null, filesFileId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesMimeType = 'fakedata';
    describe('#driveFilesExport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesExport(null, null, null, null, null, null, null, filesFileId, filesMimeType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesExport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commentsFileId = 'fakedata';
    const commentsDriveCommentsCreateBodyParam = {
      anchor: 'string',
      author: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      content: 'string',
      createdTime: 'string',
      deleted: true,
      htmlContent: 'string',
      id: 'string',
      kind: 'drive#comment',
      modifiedTime: 'string',
      quotedFileContent: {
        mimeType: 'string',
        value: 'string'
      },
      replies: [
        {
          action: 'string',
          author: {
            displayName: 'string',
            emailAddress: 'string',
            kind: 'drive#user',
            me: false,
            permissionId: 'string',
            photoLink: 'string'
          },
          content: 'string',
          createdTime: 'string',
          deleted: true,
          htmlContent: 'string',
          id: 'string',
          kind: 'drive#reply',
          modifiedTime: 'string'
        }
      ],
      resolved: true
    };
    describe('#driveCommentsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveCommentsCreate(null, null, null, null, null, null, null, commentsFileId, commentsDriveCommentsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comments', 'driveCommentsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveCommentsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveCommentsList(null, null, null, null, null, null, null, commentsFileId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comments', 'driveCommentsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commentsCommentId = 'fakedata';
    const commentsDriveCommentsUpdateBodyParam = {
      anchor: 'string',
      author: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      content: 'string',
      createdTime: 'string',
      deleted: true,
      htmlContent: 'string',
      id: 'string',
      kind: 'drive#comment',
      modifiedTime: 'string',
      quotedFileContent: {
        mimeType: 'string',
        value: 'string'
      },
      replies: [
        {
          action: 'string',
          author: {
            displayName: 'string',
            emailAddress: 'string',
            kind: 'drive#user',
            me: true,
            permissionId: 'string',
            photoLink: 'string'
          },
          content: 'string',
          createdTime: 'string',
          deleted: true,
          htmlContent: 'string',
          id: 'string',
          kind: 'drive#reply',
          modifiedTime: 'string'
        }
      ],
      resolved: true
    };
    describe('#driveCommentsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveCommentsUpdate(null, null, null, null, null, null, null, commentsFileId, commentsCommentId, commentsDriveCommentsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comments', 'driveCommentsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveCommentsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveCommentsGet(null, null, null, null, null, null, null, commentsFileId, commentsCommentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comments', 'driveCommentsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repliesFileId = 'fakedata';
    const repliesCommentId = 'fakedata';
    const repliesDriveRepliesCreateBodyParam = {
      action: 'string',
      author: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      content: 'string',
      createdTime: 'string',
      deleted: true,
      htmlContent: 'string',
      id: 'string',
      kind: 'drive#reply',
      modifiedTime: 'string'
    };
    describe('#driveRepliesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRepliesCreate(null, null, null, null, null, null, null, repliesFileId, repliesCommentId, repliesDriveRepliesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Replies', 'driveRepliesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveRepliesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRepliesList(null, null, null, null, null, null, null, repliesFileId, repliesCommentId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Replies', 'driveRepliesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repliesReplyId = 'fakedata';
    const repliesDriveRepliesUpdateBodyParam = {
      action: 'string',
      author: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: false,
        permissionId: 'string',
        photoLink: 'string'
      },
      content: 'string',
      createdTime: 'string',
      deleted: true,
      htmlContent: 'string',
      id: 'string',
      kind: 'drive#reply',
      modifiedTime: 'string'
    };
    describe('#driveRepliesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRepliesUpdate(null, null, null, null, null, null, null, repliesFileId, repliesCommentId, repliesReplyId, repliesDriveRepliesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Replies', 'driveRepliesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveRepliesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRepliesGet(null, null, null, null, null, null, null, repliesFileId, repliesCommentId, repliesReplyId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Replies', 'driveRepliesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsFileId = 'fakedata';
    const permissionsDrivePermissionsCreateBodyParam = {
      allowFileDiscovery: true,
      deleted: true,
      displayName: 'string',
      domain: 'string',
      emailAddress: 'string',
      expirationTime: 'string',
      id: 'string',
      kind: 'drive#permission',
      pendingOwner: true,
      permissionDetails: [
        {
          inherited: true,
          inheritedFrom: 'string',
          permissionType: 'string',
          role: 'string'
        }
      ],
      photoLink: 'string',
      role: 'string',
      teamDrivePermissionDetails: [
        {
          inherited: false,
          inheritedFrom: 'string',
          role: 'string',
          teamDrivePermissionType: 'string'
        }
      ],
      type: 'string',
      view: 'string'
    };
    describe('#drivePermissionsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.drivePermissionsCreate(null, null, null, null, null, null, null, permissionsFileId, null, null, null, null, null, null, null, null, permissionsDrivePermissionsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'drivePermissionsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#drivePermissionsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.drivePermissionsList(null, null, null, null, null, null, null, permissionsFileId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'drivePermissionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsPermissionId = 'fakedata';
    const permissionsDrivePermissionsUpdateBodyParam = {
      allowFileDiscovery: true,
      deleted: false,
      displayName: 'string',
      domain: 'string',
      emailAddress: 'string',
      expirationTime: 'string',
      id: 'string',
      kind: 'drive#permission',
      pendingOwner: false,
      permissionDetails: [
        {
          inherited: false,
          inheritedFrom: 'string',
          permissionType: 'string',
          role: 'string'
        }
      ],
      photoLink: 'string',
      role: 'string',
      teamDrivePermissionDetails: [
        {
          inherited: false,
          inheritedFrom: 'string',
          role: 'string',
          teamDrivePermissionType: 'string'
        }
      ],
      type: 'string',
      view: 'string'
    };
    describe('#drivePermissionsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.drivePermissionsUpdate(null, null, null, null, null, null, null, permissionsFileId, permissionsPermissionId, null, null, null, null, null, permissionsDrivePermissionsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'drivePermissionsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#drivePermissionsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.drivePermissionsGet(null, null, null, null, null, null, null, permissionsFileId, permissionsPermissionId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'drivePermissionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const revisionsFileId = 'fakedata';
    describe('#driveRevisionsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRevisionsList(null, null, null, null, null, null, null, revisionsFileId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Revisions', 'driveRevisionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const revisionsRevisionId = 'fakedata';
    const revisionsDriveRevisionsUpdateBodyParam = {
      exportLinks: {},
      id: 'string',
      keepForever: false,
      kind: 'drive#revision',
      lastModifyingUser: {
        displayName: 'string',
        emailAddress: 'string',
        kind: 'drive#user',
        me: true,
        permissionId: 'string',
        photoLink: 'string'
      },
      md5Checksum: 'string',
      mimeType: 'string',
      modifiedTime: 'string',
      originalFilename: 'string',
      publishAuto: false,
      published: true,
      publishedLink: 'string',
      publishedOutsideDomain: true,
      size: 'string'
    };
    describe('#driveRevisionsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRevisionsUpdate(null, null, null, null, null, null, null, revisionsFileId, revisionsRevisionId, revisionsDriveRevisionsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Revisions', 'driveRevisionsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveRevisionsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRevisionsGet(null, null, null, null, null, null, null, revisionsFileId, revisionsRevisionId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Revisions', 'driveRevisionsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamdrivesRequestId = 'fakedata';
    const teamdrivesDriveTeamdrivesCreateBodyParam = {
      backgroundImageFile: {
        id: 'string',
        width: 3,
        xCoordinate: 5,
        yCoordinate: 5
      },
      backgroundImageLink: 'string',
      capabilities: {
        canAddChildren: true,
        canChangeCopyRequiresWriterPermissionRestriction: true,
        canChangeDomainUsersOnlyRestriction: true,
        canChangeTeamDriveBackground: true,
        canChangeTeamMembersOnlyRestriction: false,
        canComment: false,
        canCopy: true,
        canDeleteChildren: false,
        canDeleteTeamDrive: false,
        canDownload: false,
        canEdit: false,
        canListChildren: false,
        canManageMembers: true,
        canReadRevisions: false,
        canRemoveChildren: true,
        canRename: true,
        canRenameTeamDrive: false,
        canShare: true,
        canTrashChildren: true
      },
      colorRgb: 'string',
      createdTime: 'string',
      id: 'string',
      kind: 'drive#teamDrive',
      name: 'string',
      orgUnitId: 'string',
      restrictions: {
        adminManagedRestrictions: false,
        copyRequiresWriterPermission: false,
        domainUsersOnly: false,
        teamMembersOnly: false
      },
      themeId: 'string'
    };
    describe('#driveTeamdrivesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveTeamdrivesCreate(null, null, null, null, null, null, null, teamdrivesRequestId, teamdrivesDriveTeamdrivesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teamdrives', 'driveTeamdrivesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveTeamdrivesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveTeamdrivesList(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teamdrives', 'driveTeamdrivesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamdrivesTeamDriveId = 'fakedata';
    const teamdrivesDriveTeamdrivesUpdateBodyParam = {
      backgroundImageFile: {
        id: 'string',
        width: 2,
        xCoordinate: 8,
        yCoordinate: 3
      },
      backgroundImageLink: 'string',
      capabilities: {
        canAddChildren: false,
        canChangeCopyRequiresWriterPermissionRestriction: false,
        canChangeDomainUsersOnlyRestriction: true,
        canChangeTeamDriveBackground: true,
        canChangeTeamMembersOnlyRestriction: true,
        canComment: false,
        canCopy: false,
        canDeleteChildren: false,
        canDeleteTeamDrive: true,
        canDownload: true,
        canEdit: true,
        canListChildren: false,
        canManageMembers: true,
        canReadRevisions: true,
        canRemoveChildren: false,
        canRename: true,
        canRenameTeamDrive: false,
        canShare: true,
        canTrashChildren: false
      },
      colorRgb: 'string',
      createdTime: 'string',
      id: 'string',
      kind: 'drive#teamDrive',
      name: 'string',
      orgUnitId: 'string',
      restrictions: {
        adminManagedRestrictions: false,
        copyRequiresWriterPermission: false,
        domainUsersOnly: true,
        teamMembersOnly: false
      },
      themeId: 'string'
    };
    describe('#driveTeamdrivesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveTeamdrivesUpdate(null, null, null, null, null, null, null, teamdrivesTeamDriveId, null, teamdrivesDriveTeamdrivesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teamdrives', 'driveTeamdrivesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveTeamdrivesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveTeamdrivesGet(null, null, null, null, null, null, null, teamdrivesTeamDriveId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teamdrives', 'driveTeamdrivesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveDrivesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveDrivesDelete(null, null, null, null, null, null, null, drivesDriveId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'driveDrivesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveFilesEmptyTrash - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesEmptyTrash(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesEmptyTrash', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveFilesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveFilesDelete(null, null, null, null, null, null, null, filesFileId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'driveFilesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveCommentsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveCommentsDelete(null, null, null, null, null, null, null, commentsFileId, commentsCommentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comments', 'driveCommentsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveRepliesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRepliesDelete(null, null, null, null, null, null, null, repliesFileId, repliesCommentId, repliesReplyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Replies', 'driveRepliesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#drivePermissionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.drivePermissionsDelete(null, null, null, null, null, null, null, permissionsFileId, permissionsPermissionId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'drivePermissionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveRevisionsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveRevisionsDelete(null, null, null, null, null, null, null, revisionsFileId, revisionsRevisionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Revisions', 'driveRevisionsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#driveTeamdrivesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.driveTeamdrivesDelete(null, null, null, null, null, null, null, teamdrivesTeamDriveId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-google_drive-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teamdrives', 'driveTeamdrivesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
