## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Google Drive. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Google Drive.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Google Drive. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">driveAboutGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, callback)</td>
    <td style="padding:15px">Gets information about the user, the user's Drive, and system capabilities.</td>
    <td style="padding:15px">{base_path}/{version}/about?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveChangesList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, pageToken, driveId, includeCorpusRemovals, includeItemsFromAllDrives, includePermissionsForView, includeRemoved, includeTeamDriveItems, pageSize, restrictToMyDrive, spaces, supportsAllDrives, supportsTeamDrives, teamDriveId, callback)</td>
    <td style="padding:15px">Lists the changes for a user or shared drive.</td>
    <td style="padding:15px">{base_path}/{version}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveChangesGetStartPageToken(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, supportsAllDrives, supportsTeamDrives, teamDriveId, callback)</td>
    <td style="padding:15px">Gets the starting pageToken for listing future changes.</td>
    <td style="padding:15px">{base_path}/{version}/changes/startPageToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveChangesWatch(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, pageToken, driveId, includeCorpusRemovals, includeItemsFromAllDrives, includePermissionsForView, includeRemoved, includeTeamDriveItems, pageSize, restrictToMyDrive, spaces, supportsAllDrives, supportsTeamDrives, teamDriveId, body, callback)</td>
    <td style="padding:15px">Subscribes to changes for a user.</td>
    <td style="padding:15px">{base_path}/{version}/changes/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveChannelsStop(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, body, callback)</td>
    <td style="padding:15px">Stop watching resources through this channel</td>
    <td style="padding:15px">{base_path}/{version}/channels/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, pageSize, pageToken, q, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Lists the user's shared drives.</td>
    <td style="padding:15px">{base_path}/{version}/drives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, requestId, body, callback)</td>
    <td style="padding:15px">Creates a new shared drive.</td>
    <td style="padding:15px">{base_path}/{version}/drives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, callback)</td>
    <td style="padding:15px">Permanently deletes a shared drive for which the user is an organizer. The shared drive cannot cont</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Gets a shared drive's metadata by ID.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, useDomainAdminAccess, body, callback)</td>
    <td style="padding:15px">Updates the metadate for a shared drive.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesHide(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, callback)</td>
    <td style="padding:15px">Hides a shared drive from the default view.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}/hide?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveDrivesUnhide(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, driveId, callback)</td>
    <td style="padding:15px">Restores a shared drive to the default view.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}/unhide?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, corpora, corpus = 'domain', driveId, includeItemsFromAllDrives, includePermissionsForView, includeTeamDriveItems, orderBy, pageSize, pageToken, q, spaces, supportsAllDrives, supportsTeamDrives, teamDriveId, callback)</td>
    <td style="padding:15px">Lists or searches files.</td>
    <td style="padding:15px">{base_path}/{version}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, enforceSingleParent, ignoreDefaultVisibility, includePermissionsForView, keepRevisionForever, ocrLanguage, supportsAllDrives, supportsTeamDrives, useContentAsIndexableText, body, callback)</td>
    <td style="padding:15px">Creates a new file.</td>
    <td style="padding:15px">{base_path}/{version}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesGenerateIds(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, count, space, type, callback)</td>
    <td style="padding:15px">Generates a set of file IDs which can be provided in create or copy requests.</td>
    <td style="padding:15px">{base_path}/{version}/files/generateIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesEmptyTrash(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, enforceSingleParent, callback)</td>
    <td style="padding:15px">Permanently deletes all of the user's trashed files.</td>
    <td style="padding:15px">{base_path}/{version}/files/trash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, enforceSingleParent, supportsAllDrives, supportsTeamDrives, callback)</td>
    <td style="padding:15px">Permanently deletes a file owned by the user without moving it to the trash. If the file belongs to</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, acknowledgeAbuse, includePermissionsForView, supportsAllDrives, supportsTeamDrives, callback)</td>
    <td style="padding:15px">Gets a file's metadata or content by ID.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, addParents, enforceSingleParent, includePermissionsForView, keepRevisionForever, ocrLanguage, removeParents, supportsAllDrives, supportsTeamDrives, useContentAsIndexableText, body, callback)</td>
    <td style="padding:15px">Updates a file's metadata and/or content. When calling this method, only populate fields in the req</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesCopy(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, enforceSingleParent, ignoreDefaultVisibility, includePermissionsForView, keepRevisionForever, ocrLanguage, supportsAllDrives, supportsTeamDrives, body, callback)</td>
    <td style="padding:15px">Creates a copy of a file and applies any requested updates with patch semantics. Folders cannot be</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesExport(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, mimeType, callback)</td>
    <td style="padding:15px">Exports a Google Workspace document to the requested MIME type and returns exported byte content. N</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveFilesWatch(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, acknowledgeAbuse, includePermissionsForView, supportsAllDrives, supportsTeamDrives, body, callback)</td>
    <td style="padding:15px">Subscribes to changes to a file. While you can establish a channel forchanges to a file on a shared</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveCommentsList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, includeDeleted, pageSize, pageToken, startModifiedTime, callback)</td>
    <td style="padding:15px">Lists a file's comments.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveCommentsCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, body, callback)</td>
    <td style="padding:15px">Creates a new comment on a file.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveCommentsDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, callback)</td>
    <td style="padding:15px">Deletes a comment.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveCommentsGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, includeDeleted, callback)</td>
    <td style="padding:15px">Gets a comment by ID.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveCommentsUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, body, callback)</td>
    <td style="padding:15px">Updates a comment with patch semantics.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRepliesList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, includeDeleted, pageSize, pageToken, callback)</td>
    <td style="padding:15px">Lists a comment's replies.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}/replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRepliesCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, body, callback)</td>
    <td style="padding:15px">Creates a new reply to a comment.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}/replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRepliesDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, replyId, callback)</td>
    <td style="padding:15px">Deletes a reply.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}/replies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRepliesGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, replyId, includeDeleted, callback)</td>
    <td style="padding:15px">Gets a reply by ID.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}/replies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRepliesUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, commentId, replyId, body, callback)</td>
    <td style="padding:15px">Updates a reply with patch semantics.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/comments/{pathv2}/replies/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">drivePermissionsList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, includePermissionsForView, pageSize, pageToken, supportsAllDrives, supportsTeamDrives, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Lists a file's or shared drive's permissions.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">drivePermissionsCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, emailMessage, enforceSingleParent, moveToNewOwnersRoot, sendNotificationEmail, supportsAllDrives, supportsTeamDrives, transferOwnership, useDomainAdminAccess, body, callback)</td>
    <td style="padding:15px">Creates a permission for a file or shared drive.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">drivePermissionsDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, permissionId, supportsAllDrives, supportsTeamDrives, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Deletes a permission.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">drivePermissionsGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, permissionId, supportsAllDrives, supportsTeamDrives, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Gets a permission by ID.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">drivePermissionsUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, permissionId, removeExpiration, supportsAllDrives, supportsTeamDrives, transferOwnership, useDomainAdminAccess, body, callback)</td>
    <td style="padding:15px">Updates a permission with patch semantics.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRevisionsList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, pageSize, pageToken, callback)</td>
    <td style="padding:15px">Lists a file's revisions.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/revisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRevisionsDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, revisionId, callback)</td>
    <td style="padding:15px">Permanently deletes a file version. You can only delete revisions for files with binary content in</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/revisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRevisionsGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, revisionId, acknowledgeAbuse, callback)</td>
    <td style="padding:15px">Gets a revision's metadata or content by ID.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/revisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveRevisionsUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, fileId, revisionId, body, callback)</td>
    <td style="padding:15px">Updates a revision with patch semantics.</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/revisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveTeamdrivesList(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, pageSize, pageToken, q, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Deprecated use drives.list instead.</td>
    <td style="padding:15px">{base_path}/{version}/teamdrives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveTeamdrivesCreate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, requestId, body, callback)</td>
    <td style="padding:15px">Deprecated use drives.create instead.</td>
    <td style="padding:15px">{base_path}/{version}/teamdrives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveTeamdrivesDelete(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, teamDriveId, callback)</td>
    <td style="padding:15px">Deprecated use drives.delete instead.</td>
    <td style="padding:15px">{base_path}/{version}/teamdrives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveTeamdrivesGet(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, teamDriveId, useDomainAdminAccess, callback)</td>
    <td style="padding:15px">Deprecated use drives.get instead.</td>
    <td style="padding:15px">{base_path}/{version}/teamdrives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">driveTeamdrivesUpdate(alt = 'json', fields, key, oauthToken, prettyPrint, quotaUser, userIp, teamDriveId, useDomainAdminAccess, body, callback)</td>
    <td style="padding:15px">Deprecated use drives.update instead</td>
    <td style="padding:15px">{base_path}/{version}/teamdrives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
