
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:09PM

See merge request itentialopensource/adapters/adapter-google_drive!11

---

## 0.5.3 [08-27-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-google_drive!9

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:18PM

See merge request itentialopensource/adapters/adapter-google_drive!8

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:31PM

See merge request itentialopensource/adapters/adapter-google_drive!7

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/persistence/adapter-google_drive!6

---

## 0.4.3 [03-29-2024]

* Changes made at 2024.03.29_10:34AM

See merge request itentialopensource/adapters/persistence/adapter-google_drive!5

---

## 0.4.2 [03-13-2024]

* Changes made at 2024.03.13_15:18PM

See merge request itentialopensource/adapters/persistence/adapter-google_drive!4

---

## 0.4.1 [02-28-2024]

* Changes made at 2024.02.28_12:45PM

See merge request itentialopensource/adapters/persistence/adapter-google_drive!3

---

## 0.4.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/persistence/adapter-google_drive!2

---

## 0.3.0 [03-20-2023]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-google_drive!1

---

## 0.2.0 [06-01-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-google_drive!1

---

## 0.1.1 [03-17-2022]

- Initial Commit

See commit 58e3fbb

---
